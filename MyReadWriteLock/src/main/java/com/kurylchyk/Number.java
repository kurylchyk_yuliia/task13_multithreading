package com.kurylchyk;

public class Number {
    private MyReadWriteLock lock = new MyReadWriteLock();
    private int num = 5;

    public void change(int value) throws InterruptedException {
        lock.lockWrite();
        num+=value;
        lock.unlockWrite();
    }


    public void get() throws InterruptedException {
        lock.lockRead();
        System.out.println(num);
        lock.unlockRead();
    }
}
