package com.kurylchyk;

public class Number {
    volatile int number;

    Number(){
        number = 0;
    }

    public int getNumber(){
        return number;
    }
}
