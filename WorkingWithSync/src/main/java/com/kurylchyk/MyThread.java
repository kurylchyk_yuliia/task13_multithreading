package com.kurylchyk;

import java.time.LocalDateTime;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class MyThread implements Runnable {
    private Thread thread;
    private Number number = new Number();
    private int forAdd;
    private int forSub;
    private int forMul;

    private Lock lock = new ReentrantLock();

    MyThread(String name,int add, int sub, int mul) {
        forAdd = add;
        forSub = sub;
        forMul = mul;
        thread = new Thread(this, name);
        thread.start();
    }


    /**
     * Methods add, subtract and multiply runs one after another
     * @param value
     */
    public void add(int value) {

        lock.lock();
            System.out.println("Start the adding " + thread.getName() + " in " + LocalDateTime.now());
            number.number += value;
            System.out.println("End the adding " + thread.getName() + " in " + LocalDateTime.now());


    }

    public void subtract(int value) {

            System.out.println("Start the subtracting " + thread.getName() + " in " + LocalDateTime.now());
            number.number -= value;
            System.out.println("End the subtracting " + thread.getName() + " in " + LocalDateTime.now());

    }

    public void multiply(int value) {

            System.out.println("Start the multiplying " + thread.getName() + " in " + LocalDateTime.now());
            number.number *= value;
            System.out.println("End the multiplying " + thread.getName() + " in " + LocalDateTime.now());

        lock.unlock();
    }
    public void run() {
        add(forAdd);
        subtract(forSub);
        multiply(forMul);
        System.out.println("Changed number: " + number.getNumber());
    }



}
