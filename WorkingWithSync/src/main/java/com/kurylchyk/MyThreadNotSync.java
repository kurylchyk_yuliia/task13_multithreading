package com.kurylchyk;

import java.time.LocalDateTime;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyThreadNotSync implements Runnable {

    private Number number1;
    private Number number2;
    private Number number3;
    private Thread thread;
    private int value;
    private Lock lock1 = new ReentrantLock();
    private Lock lock2 = new ReentrantLock();
    private Lock lock3 = new ReentrantLock();

    MyThreadNotSync(String name, int value) {

        thread = new Thread(this, name);
        number1 = new Number();
        number2 = new Number();
        number3 = new Number();
        this.value = value;
        thread.start();
    }

    /**
     * 3 next methods executes in the same time
     *
     * @param value
     */
    public void changeFirst(int value) {
        lock1.lock();
            System.out.println("Start the changing " + thread.getName() + " in " + LocalDateTime.now());

            number1.number += value;

            System.out.println("End the changing " + thread.getName() + " in " + LocalDateTime.now());
        lock1.unlock();

    }

    public void changeSecond(int value) {

       lock2.lock();
            System.out.println("Start the changing " + thread.getName() + " in " + LocalDateTime.now());

            number2.number += value;
            System.out.println("End the changing " + thread.getName() + " in " + LocalDateTime.now());

        lock2.unlock();
    }

    public void changeThird(int value) {

       lock3.lock();
            System.out.println("Start the changing " + thread.getName() + " in " + LocalDateTime.now());
            number3.number += value;
            System.out.println("End the changing " + thread.getName() + " in " + LocalDateTime.now());
        lock3.unlock();
    }

    public void run() {

        changeFirst(value);
        changeSecond(value);
        changeThird(value);
        System.out.println("Changed number: " + number1.getNumber());
        System.out.println("Changed number: " + number2.getNumber());
        System.out.println("Changed number: " + number3.getNumber());

    }


}
