package com.kurylchyk;

public class MyThread implements  Runnable{

    private int delay;

    MyThread(int delay) {
        this.delay = delay;
    }
    public void run() {
        System.out.println("You have been waiting for " +delay + " seconds" );
    }
}
