package com.kurylchyk;

import java.sql.Time;
import java.util.Random;
import java.util.concurrent.*;
public class Application {
    private static Random random = new Random();
    private static int delay;

    public static void main(String[] args) {

        Integer size  = Integer.parseInt(args[0]);
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(size);

        int index = 0;
        while(index<size) {
            generate();
            pool.schedule(new MyThread(delay),delay,TimeUnit.SECONDS);
            index++;
        }

        pool.shutdown();
    }

    private static void generate(){

        delay = random.nextInt(10)+1;
    }
}
