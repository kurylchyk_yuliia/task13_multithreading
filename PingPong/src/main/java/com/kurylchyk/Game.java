package com.kurylchyk;


public class Game {

    private View view = (s)-> System.out.println(s);
    private Ball ball = new Ball();


    public void play() {
        Thread t1 = new Thread(() -> {
            synchronized (ball) {
                for (int i = 1; i <= 10; i++) {
                    try {
                       view.show("Player 1: throwing the "+ball);
                       ball.wait();
                    } catch (InterruptedException e) {
                    }
                    ball.notify();
                    view.show("Player 1 : catching the "+ ball);
                }
            }
        });
        Thread t2 = new Thread(() -> {
            synchronized (ball) {
                for (int i = 1; i <= 10; i++) {
                    ball.notify();
                    view.show("Player 2 : catching the " +ball);
                    try {
                        view.show("Player 2 : throwing the " +ball);
                        ball.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        });

        view.show("Starting the game1");
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
        }
        view.show("Finishing!");
    }

}
