package com.kurylchyk;

public class FibonacciNumbers {


    private int[] Fibonacci;

    FibonacciNumbers(int size){
        Fibonacci = new int[size];
        getNumbers();
    }


    private void getNumbers(){

        Fibonacci[0] = 1;
        Fibonacci[1] = 1;
        for(int index = 2; index<Fibonacci.length; index++ ){
            Fibonacci[index]= Fibonacci[index-1]+Fibonacci[index-2];
        }
    }

    public synchronized  String show(){

        String allFibonacci ="";
        for(int element: Fibonacci) {
            allFibonacci+=element+"\t";
        }
        allFibonacci+="\n";
        return allFibonacci;
    }

    public int getElement(int index) {
        return Fibonacci[index];
    }
}
