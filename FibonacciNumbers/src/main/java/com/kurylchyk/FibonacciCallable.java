package com.kurylchyk;

import java.util.Scanner;
import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Integer> {
    private Integer numberOfFibonacci;
    private View view = (s)->{
        System.out.println(s);
    };
    private static Scanner sc = new Scanner(System.in);
    private FibonacciNumbers fibonacciNumbers;

    private  void getNumber(){
        System.out.println("Enter the number of fibonacci for Callable");
       numberOfFibonacci = sc.nextInt();
    }

    public Integer call(){
        getNumber();
        fibonacciNumbers = new FibonacciNumbers(numberOfFibonacci);
        int sum = 0;

        for(int index = 0; index< numberOfFibonacci; index++){
            sum+=fibonacciNumbers.getElement(index);
        }
        return sum;

    }


}
