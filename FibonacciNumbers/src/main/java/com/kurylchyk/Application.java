package com.kurylchyk;

import java.util.Scanner;
import java.util.concurrent.*;

public class Application {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException, ExecutionException {


        ExecutorService executorService = Executors.newSingleThreadExecutor();
        MyThread first = new MyThread("First");
        executorService.submit(first);

        executorService.shutdown();
        ExecutorService bigExecutor  = Executors.newFixedThreadPool(2);

        MyThread second = new MyThread("second");
        bigExecutor.submit(second);
        MyThread third = new MyThread("third");
        bigExecutor.submit(third);
        bigExecutor.shutdown();
        //-------------using callable
        Callable<Integer> fb1 = new FibonacciCallable();
        FutureTask futureTask1 =  new FutureTask(fb1);
        new Thread(futureTask1).start();
        System.out.println("The sum of FB numbers is : "+ futureTask1.get());

        Callable<Integer> fb2 = new FibonacciCallable();
        FutureTask futureTask2 =  new FutureTask(fb2);
        new Thread(futureTask2).start();
        System.out.println("The sum of FB numbers is : "+ futureTask2.get());

        Callable<Integer> fb3 = new FibonacciCallable();
        FutureTask futureTask3 =  new FutureTask(fb3);
        new Thread(futureTask3).start();
        System.out.println("The sum of FB numbers is : "+ futureTask3.get());
    }
}
