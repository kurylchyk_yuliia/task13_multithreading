package com.kurylchyk;

import java.util.Scanner;

public class MyThread implements Runnable {
    private static Scanner sc = new Scanner(System.in);
    Thread thread;
    private FibonacciNumbers fibonacci;
    private int numberOfFibonacci;
    private View view = (s) -> {
        System.out.println(s);
    };


    MyThread(String name) {
        thread = new Thread(this, name);
        getNumber();
       // thread.start();
        //because of executor


    }

    public synchronized void getNumber() {
        view.show("Enter the number of fibonacci: ");
        numberOfFibonacci = sc.nextInt();
    }

    public synchronized void run() {

            view.show("Running the thread " + thread.getName());
            fibonacci = new FibonacciNumbers(numberOfFibonacci);
            view.show(fibonacci.show());

            try {
                thread.sleep(500);
            }catch(InterruptedException ex){
                System.out.println("The thread " +thread.getName() +" is finished");
                return;
            }
        }

    }

