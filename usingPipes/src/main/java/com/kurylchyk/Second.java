package com.kurylchyk;


import java.io.IOException;
import java.io.PipedInputStream;
import java.util.concurrent.BlockingQueue;

public class Second implements Runnable {
    private BlockingQueue<Integer> queue;

    public Second(BlockingQueue<Integer> q) {
        queue = q;
    }


    public void run() {
        int value = 0;
        int index = 0;
        while (true) {
            try {
                if (index > 25) break;
                Thread.sleep(3000);
                value = queue.take();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Taking ->" + value);
            index++;
        }
    }
}



