package com.kurylchyk;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

public class Application {
    public static void main(String[] args) throws IOException {

        BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(2);

        First first = new First(queue);
        Second second = new Second(queue);

        Thread one =  new Thread(first);
        Thread two = new Thread(second);

        one.start();
        two.start();

    }
}
