package com.kurylchyk;

import java.io.IOException;

import java.io.PipedOutputStream;
import java.util.concurrent.BlockingQueue;


public class First implements Runnable {
    private BlockingQueue<Integer> queue;

    First(BlockingQueue<Integer> q) {
        queue = q;
    }

    public void run() {
        int index = 0;
        while (index <= 25) {

            try {

                Thread.sleep(1000);
                queue.put(index);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Adding ->" + index);
            index++;
        }

    }

}


